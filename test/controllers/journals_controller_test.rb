require 'test_helper'

class JournalsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @journal = journals(:lab_journal)
    @other_journal = journals(:character_notes)
    @public_journal = journals(:dream_diary)
    @drafts = journals(:jesses_drafts)
    @walter = authors(:walter_white)
  end

  test "should get index" do
    get journals_url
    assert_response :success
  end

  test "should get my journals" do
      sign_in authors(:walter_white)
      get journals_url
      assert_response :success
  end

  test "should get new" do
    sign_in authors(:walter_white)
    get new_author_journal_url(@walter.id)
    assert_response :success
  end

  test "should not get new" do
    get new_author_journal_url(@walter.id)
    assert_response :unauthorized
  end

  test "should create journal" do
    sign_in authors(:walter_white)
    assert_difference('Journal.count') do
      post journals_url, params: { journal: { title: 'My Test Journal' } }
    end

    assert_redirected_to journal_url(Journal.last)
  end

  test "should show public journal" do
    get journal_url(@public_journal)
    assert_response :success
  end

  test "should not show private journal to stranger" do
    get journal_url(@other_journal)
    assert_response :forbidden
  end

  test "should not show private journal to another author" do
    sign_in authors(:jesse_pinkman)
    get journal_url(@other_journal)
    assert_response :forbidden
  end

  test "should get edit" do
    sign_in authors(:walter_white)
    get edit_journal_url(@journal)
    assert_response :success
  end

  test "should update journal" do
    sign_in authors(:walter_white)
    patch journal_url(@journal), params: { journal: { title: 'Hello' } }
    assert_redirected_to journal_url(@journal)
  end

  test "should destroy journal" do
    sign_in authors(:walter_white)

    assert_difference('Journal.count', -1) do
      delete journal_url(@journal)
    end

    assert_redirected_to @journal.author
  end

  test "should not get edit" do
    sign_in authors(:jesse_pinkman)
    get edit_journal_url(@journal)
    assert_response :forbidden
  end

  test "should not update journal" do
    sign_in authors(:jesse_pinkman)
    patch journal_url(@journal), params: { journal: { title: 'Hello' } }
    assert_response :forbidden
  end

  test "should not destroy journal" do
    sign_in authors(:jesse_pinkman)
    assert_difference('Journal.count', 0) do
      delete journal_url(@journal)
    end

    assert_response :forbidden
  end

  test "should coerce entries visibility" do
    sign_in authors(:walter_white)
    put journal_url(@journal), params: { journal: { title: "I updated my visibility.", visibility: "internet" } }
    @journal.reload
    assert @journal.entries.all? {|e| e.visibility == @journal.visibility}
  end

  test "should not destroy locked journal" do
    sign_in authors(:jesse_pinkman)
    assert_difference('Journal.count', 0) do
      delete journal_url(@drafts), as: :json
    end

    assert_response :unprocessable_entity
  end
end
