require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @entry = entries(:steps_to_happiness)
    @comment = comments(:positive)
    @author = authors(:walter_white)
  end

  test "should get index" do
    get comments_url
    assert_response :success
  end

  test "should get new" do
    sign_in authors(:walter_white)
    get new_entry_comment_url(@entry)
    assert_response :success
  end

  test "should create comment" do
    sign_in authors(:walter_white)

    assert_difference('Comment.count') do
      post entry_comments_url(@entry), params: { comment: { body: @comment.body, entry_id: @entry.id } }
    end

    assert_redirected_to entry_url(@entry)
  end

  test "should show comment" do
    sign_in authors(:walter_white)
    get comment_url(@comment)
    assert_response :success
  end

  test "should get edit" do
    sign_in authors(:walter_white)
    get edit_comment_url(@comment)
    assert_response :success
  end

  test "should update comment" do
    sign_in authors(:walter_white)
    patch comment_url(@comment), params: { comment: { body: @comment.body } }
    assert_redirected_to entry_url(@entry)
  end

  test "should destroy comment" do
    sign_in authors(:walter_white)
    assert_difference('Comment.count', -1) do
      delete comment_url(@comment)
    end

    assert_redirected_to comments_url
  end
end
