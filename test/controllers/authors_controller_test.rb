require 'test_helper'

class AuthorsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @author = authors(:walter_white)
  end

  test "should get index" do
    get authors_url
    assert_response :success
  end

  test "should get new" do
    get new_author_url
    assert_response :success
  end

  test "should create author" do
    assert_difference('Author.count') do
      post authors_url, params: { author:
          { name: 'Tester McTesterson', email: 'uru.nova+tester@gmail.com',
            password: 'password', password_confirmation: 'password' } }
    end

    assert_redirected_to root_url
  end

  test "should show author" do
    get author_url(@author)
    assert_response :success
  end

  test "should get edit" do
    sign_in authors(:walter_white)
    get edit_author_registration_url(@author)
    assert_response :success
  end

  test "should destroy author" do
    sign_in authors(:walter_white)
    assert_difference('Author.count', -1) do
      delete authors_url
    end

    assert_redirected_to root_url
  end

end
