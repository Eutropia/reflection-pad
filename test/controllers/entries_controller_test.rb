require 'test_helper'

class EntriesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @entry = entries(:lab_20120415)
    @journal = journals(:lab_journal)
    @secret_journal = journals(:character_notes)
  end

  test "should get index" do
    get journal_entries_url(journal_id: @journal.id)
    assert_response :success
  end

  test "should get new" do
    sign_in authors(:walter_white)
    get new_entry_url
    assert_redirected_to edit_entry_url(Entry.last)
  end

  test "should show entry" do
    sign_in authors(:walter_white)
    get entry_url(@entry)
    assert_response :success
  end

  test "should update entry" do
    sign_in authors(:walter_white)
    patch entry_url(@entry), params: { entry: { body: "I wrote some better rails code today." } }
    assert_redirected_to journal_url(@entry.journal)
  end

  test "should destroy entry" do
    sign_in authors(:walter_white)
    assert_difference('Entry.count', -1) do
      delete entry_url(@entry)
    end

    assert_redirected_to @entry.journal
  end

  test "should not get new" do
    get new_entry_url
    assert_redirected_to author_session_url
  end

  test "should not update entry" do
    sign_in authors(:jesse_pinkman)
    patch entry_url(@entry), params: { entry: { body: "I wrote some better rails code today." } }
    assert_response :forbidden
  end

  test "should not destroy entry" do
    sign_in authors(:jesse_pinkman)
    assert_difference('Entry.count', 0) do
      delete entry_url(@entry)
    end

    assert_response :forbidden
  end

  test "should have visibility equal to journal" do
    sign_in authors(:walter_white)
    put entry_url(@entry), params: { entry: { body: "I updated my visibility.", journal_id: @secret_journal.id } }
    @entry.reload
    assert_equal(@entry.visibility, @secret_journal.visibility)
  end
end
