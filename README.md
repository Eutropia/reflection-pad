# Reflection Pad ReadMe
The source for https://reflectionpad.com

A few notes, at least.

* NGINX Reverse Proxy
* Postgres
* Rails 5.1 on Ruby 2.4.1 with Puma server
* Plain JS
* ConciseCSS Framework
* Capistrano for deployment

Pretty much vanilla rails.  The goal was to make it go fast, however -- hence the stripped-down font set, the small css framework, and the lack of jquery or any other JS library.

Feel free to log in and play around!

Hopefully it's secure with the settings I gave it, but I'm no expert.
