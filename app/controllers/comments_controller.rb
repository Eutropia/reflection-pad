class CommentsController < ApplicationController
  before_action :authenticate_author!, only: [:edit, :update, :destroy, :new, :create]
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.where(entry_id: params[:entry_id]).all
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    unless @comment.view_permitted?(current_author)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end
  end

  # GET entries/1/comments/new
  def new
    unless current_author
      render(:file => File.join(Rails.root, 'public/401.html'), :status => 401, :layout => false)
    else
      entry = Entry.find(params[:entry_id])

      unless entry
        respond_to do |format|
          format.html { redirect_to root_url, alert: 'Can\'t write a comment for an entry that doesn\'t exist' , status: 303}
          format.json { head :unprocessable_entity }
        end
      else
        @comment = Comment.new(entry_id: entry.id, author_id: current_author.try(:id), body: params[:body] || 'I like this.')
      end
    end
  end

  # GET /comments/1/edit
  def edit
    unless current_author && @comment.author.id == current_author.try(:id)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end
  end

  # POST entries/1/comments
  # POST entries/1/comments.json
  def create
    unless current_author
      render(:file => File.join(Rails.root, 'public/401.html'), :status => 401, :layout => false)
      return false
    end

    entry = Entry.find(comment_params[:entry_id])

    unless entry
      respond_to do |format|
        format.html { redirect_to root_url, alert: 'Can\'t write a comment for an entry that doesn\'t exist' , status: 303}
        format.json { head :unprocessable_entity }
      end
      return false
    end

    @comment = Comment.new(
      comment_params.merge(author: current_author, visibility: entry.visibility, entry: entry)
    )

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @comment.entry, notice: 'Comment was successfully saved.' , status: 303}
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    unless current_author && @comment.author_id == current_author.try(:id)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return false
    end

    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment.entry, notice: 'Comment was successfully saved.' , status: 303}
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    unless current_author && @comment.author_id == current_author.try(:id)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return false
    end
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url, notice: 'Comment was successfully deleted.' , status: 303}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:body, :entry_id, :visibility)
    end
end
