class EntriesController < ApplicationController
  before_action :authenticate_author!, only: [:edit, :update, :destroy, :new, :create]
  before_action :set_entry, only: [:show, :edit, :update, :destroy]


  # TODO: dry out auth checks for methods. too tired to do it now.

  # GET /entries
  # GET /entries.json
  def index
    @entries = Entry.where(journal: params[:journal_id]).all
  end

  # GET /entries/1
  # GET /entries/1.json
  def show
    unless @entry.view_permitted?(current_author)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end
  end

  # GET /entries/new
  def new
    unless current_author
      render(:file => File.join(Rails.root, 'public/401.html'), :status => 401, :layout => false)
    else
      @entry = Entry.create( author: current_author, journal_id: current_author.draft_journal.id )
      Rails.logger.info("Entry: #{@entry.errors.full_messages}")
      redirect_to edit_entry_path(@entry), status: 303
    end
  end

  # GET /entries/1/edit
  def edit
    unless current_author && @entry.author.id == current_author.try(:id)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end
  end

# POST /entries
  # POST /entries.json
  def create
    unless current_author
      render(:file => File.join(Rails.root, 'public/401.html'), :status => 401, :layout => false)
      return false
    end
    @entry = Entry.new(entry_params)

    respond_to do |format|
      if @entry.save
        format.html { redirect_to @entry, notice: 'Entry was successfully created.', status: 303 }
        format.json { render :show, status: :created, location: @entry }
      else
        format.html { render :new }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entries/1
  # PATCH/PUT /entries/1.jsonz
  def update
    unless current_author && @entry.author.id == current_author.try(:id)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return false
    end

    respond_to do |format|
      if @entry.update(entry_params)
        format.html { redirect_to @entry.journal, notice: 'Entry was successfully updated.', status: 303 }
        format.json { render :show, status: :ok, location: @entry }
      else
        format.html { render :edit }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entries/1
  # DELETE /entries/1.json
  def destroy
    unless current_author && @entry.author.id == current_author.try(:id)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return false
    end

    @entry.destroy
    respond_to do |format|
      format.html { redirect_to @entry.journal, notice: 'Entry was successfully deleted.', status: 303 }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entry
      @entry = Entry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_params
      # Be extra paranoid with entries because we override the default html rendering
      white_list_sanitizer = Rails::Html::WhiteListSanitizer.new
      full_sanitizer = Rails::Html::FullSanitizer.new
      scrubbed_params = params.require(:entry).permit(:body, :journal_id, :title)
      scrubbed_params[:body] = white_list_sanitizer.sanitize(scrubbed_params[:body]) if scrubbed_params[:body]
      scrubbed_params[:title] = full_sanitizer.sanitize(scrubbed_params[:title]) if scrubbed_params[:title]

      return scrubbed_params
    end
end
