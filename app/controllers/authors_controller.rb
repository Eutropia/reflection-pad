class AuthorsController < ApplicationController
  before_action :authenticate_author!, only: [:edit, :update, :destroy]
  before_action :set_author, only: [:show, :edit, :update, :destroy]

  # GET /authors
  # GET /authors.json
  def index
    @authors = Author.all
  end

  # GET /authors/1
  # GET /authors/1.json
  def show
  end

  # GET /authors/new
  def new
    @author = Author.new
  end

  # GET /authors/1/edit
  def edit
    unless current_author && current_author == @author
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end
  end

  # PATCH/PUT /authors/1
  # PATCH/PUT /authors/1.json
  def update
    unless current_author && current_author == @author
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return false
    end
    respond_to do |format|
      if @author.update(author_params)
        format.html { redirect_to @author, status: 303, notice: 'Author was successfully updated.' }
        format.json { render :show, status: :ok, location: @author }
      else
        format.html { render :edit }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
  end

  def my_data
    unless current_author
      render(:file => File.join(Rails.root, 'public/401.html'), :status => 401, :layout => false)
      return false
    end

    data = {
      account: current_author.attributes.slice('name', 'bio', 'created_at', 'active_at', 'email'),
      journals: current_author.journals.map { |j| { j.title.to_sym => j.attributes, entries: j.entries.map(&:attributes) } },
      comments: current_author.comments.all.map(&:attributes)
    }

    filename = "#{current_author.name}.json"
    temp_file = Tempfile.new()
    File.open(temp_file, 'w+') do |f|
      f << JSON.pretty_generate(data)
    end

    send_file(temp_file, filename: filename, type: :json)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_author
      @author = Author.find(params[:id])
    end
end
