class JournalsController < ApplicationController
  before_action :authenticate_author!, only: [:edit, :update, :destroy]
  before_action :set_journal, only: [:show, :edit, :update, :destroy]

  # GET /journals
  # GET /journals.json
  def index
    @journals = Journal.allowed(current_author).all
  end

  # GET /journals/1
  # GET /journals/1.json
  def show
    unless @journal.view_permitted?(current_author)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end
  end

  # GET /journals/new
  def new
    unless current_author
      render(:file => File.join(Rails.root, 'public/401.html'), :status => 401, :layout => false)
    else
      @journal = Journal.new(author: current_author, title: "My Newest Journal")
    end
  end

  # GET /journals/1/edit
  def edit
    unless current_author && @journal.author_id == current_author.try(:id)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
    end
  end

  # POST /journals
  # POST /journals.json
  def create
    unless current_author
      render(:file => File.join(Rails.root, 'public/401.html'), :status => 401, :layout => false)
      return false
    end

    @journal = Journal.new(journal_params)
    @journal.author_id = current_author.try(:id)

    respond_to do |format|
      if @journal.save
        format.html { redirect_to @journal, notice: 'Journal was successfully created.', status: 303 }
        format.json { render :show, status: :created, location: @journal }
      else
        format.html { render :new }
        format.json { render json: @journal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /journals/1
  # PATCH/PUT /journals/1.json
  def update
    unless current_author && @journal.author_id == current_author.try(:id)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return false
    end
    respond_to do |format|
      if @journal.update(journal_params)
        format.html { redirect_to @journal, notice: 'Journal was successfully updated.', status: 303 }
        format.json { render :show, status: :ok, location: @journal }
      else
        format.html { render :edit }
        format.json { render json: @journal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /journals/1
  # DELETE /journals/1.json
  def destroy
    unless current_author && @journal.author_id == current_author.try(:id)
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
      return false
    end
    if @journal.locked == true
      respond_to do |format|
        format.html { redirect_to current_author, alert: 'Cannot destroy drafts journal', status: 303 }
        format.json { head :unprocessable_entity }
      end
      return false
    end
    @journal.destroy
    respond_to do |format|
      format.html { redirect_to current_author, notice: 'Journal was successfully deleted.', status: 303 }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_journal
      @journal = Journal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def journal_params
      params.require(:journal).permit(:title, :visibility)
    end
end
