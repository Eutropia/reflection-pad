module Visibility
  extend ActiveSupport::Concern
  
  class_methods do

    def allowed(requester)
      if requester.nil?
        return where(visibility: 2)
      else
        return where("(visibility = 0 AND author_id = ?) OR (visibility in (?))", requester.try(:id), [1,2])
      end
    end
  end

  def view_permitted?(requester)
    return true if internet?
    return true if requester && ['internal', 'internet'].member?(visibility)
    return true if requester == author && (visibility == 'secret')
    return false
  end
end
