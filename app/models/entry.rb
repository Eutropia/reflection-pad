class Entry < ApplicationRecord
  include Visibility

  enum visibility: { secret: 0, internal: 1, internet: 2 }

  belongs_to :journal
  has_one :author, through: :journal
  has_many :comments, dependent: :destroy

  validates_presence_of :journal
  validates :visibility, :inclusion => { :in => Proc.new {|entry| [entry.journal.visibility] } }

  before_validation :ensure_visibility_matches_journal
  after_save :update_activity

  private

  def update_activity
    journal.touch :active_at
    author.touch :active_at
  end

  def ensure_visibility_matches_journal
    self.visibility = journal.visibility
  end
end
