class Comment < ApplicationRecord
  include Visibility

  belongs_to :author
  belongs_to :entry
  enum visibility: { secret: 0, internal: 1, internet: 2 }

  validates :visibility, :inclusion => { :in => Proc.new {|comment| [comment.entry.visibility] } }
  validates_presence_of :entry, :author

  before_validation :ensure_visibility_matches_entry
  after_save :update_activity

  private
  def update_activity
    author.touch :active_at
  end

  def ensure_visibility_matches_entry
    self.visibility = entry.visibility
  end
end
