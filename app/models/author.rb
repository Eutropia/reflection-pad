class Author < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :journals, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :entries, through: :journals, dependent: :destroy

  after_create :create_drafts_journal
  before_destroy Proc.new {|a| a.draft_journal.try(:destroy)}

  def draft_journal
    journals.where(locked: true).first
  end

  private
  def create_drafts_journal
    journals.create(visibility: 'secret', title: 'Drafts', locked: true)
  end
end
