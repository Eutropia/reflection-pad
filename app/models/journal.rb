class Journal < ApplicationRecord
  include Visibility

  enum visibility: { secret: 0, internal: 1, internet: 2 }

  belongs_to :author
  has_many :entries, dependent: :destroy
  has_many :comments, through: :entries

  before_save :ensure_entries_share_visibility

  def title
    return attributes['title'].blank? ? 'Untitled' : attributes['title']
  end

  private

  def ensure_entries_share_visibility
    entries.update_all(visibility: visibility)
    comments.update_all(visibility: visibility)
  end
end
