//= require rails-ujs
//= require local_time
//= require turbolinks
//= require_tree .

function topnav() {
  var menu = document.querySelectorAll('nav ul')[0];
  var items = document.querySelectorAll('nav ul li');
  var spans = document.querySelectorAll('nav ul li a span')

  function checknav() {
    var width = window.screen.width;

    if (width > 480) {
      removeClass(items, 'fa-2x');
      removeClass(spans, '_pls');

      if (getComputedStyle(menu)['display'] == 'none') {
        menu.style.display = '';
      }
    } else {
      addClass(spans, '_pls');
      addClass(items, 'fa-2x');
    }
  }

  function addClass(el, className) {
    if (el.classList)
      el.classList.add(className);
    else
      el.className += ' ' + className;
    }

  function removeClass(elements, className) {
    Array.prototype.forEach.call(elements, function(el, i){
      el.addEventListener('click', function(e) {
        if (el.classList)
          el.classList.remove(className);
        else
          el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        });
      });
    }

  document.querySelectorAll('#menutoggle')[0].addEventListener('click', function(e) {
    if (getComputedStyle(menu)['display'] == 'none') {
      menu.style.display = 'table';
    } else {
      menu.style.display = 'none';
    }
  });

  var nav_lis = document.querySelectorAll('nav li')
  Array.prototype.forEach.call(nav_lis, function(el, i){
    el.addEventListener('click', function(e) {
        var width = window.screen.width;
        if (width < 480) {
          menu.style.display = 'none';
        }
      });
    });


  window.addEventListener('resize', checknav);
  window.addEventListener('orientationchange', checknav);
  checknav();
}


document.addEventListener('turbolinks:load', function() {
  // Ugly spot to leave js like this, but fine for now.
  topnav();
  if (document.querySelectorAll("#entry-editor").length > 0) {
    ZenPen.editor.init();
  }
})
