/*
The MIT License (MIT)
Copyright (c) 2016 ~ Tim Holman - @twholman

Permission is hereby granted, free of charge, to any person obtaining a copy of  this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions: The above copyright notice and this
permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
// Thank you Tim for your beautiful text editor.  Hope you don't mind me incorporating it into a
//  journaling app with a few modifications -- Andrew
// Utility functions
ZenPen = window.ZenPen || {};
ZenPen.util = (function() {

	function supportsHtmlStorage() {
		try {
		    return 'localStorage' in window && window['localStorage'] !== null;
		} catch (e) {
		    return false;
		}
	};

	function getText(el) {
		var ret = " ";
		var length = el.childNodes.length;
		for(var i = 0; i < length; i++) {
		    var node = el.childNodes[i];
		    if(node.nodeType != 8) {

			if ( node.nodeType != 1 ) {
			    // Strip white space.
			    ret += node.nodeValue;
			} else {
			    ret += getText( node );
			}
		    }
		}
		return ZenPen.util.trim(ret);
	};

	function trim(string) {
		return string.replace(/^\s+|\s+$/g, '');
	};

	return {
		trim: trim,
		getText: getText,
		supportsHtmlStorage: supportsHtmlStorage
	}

})()
