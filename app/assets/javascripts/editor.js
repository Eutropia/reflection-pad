/*
The MIT License (MIT)
Copyright (c) 2016 ~ Tim Holman - @twholman

Permission is hereby granted, free of charge, to any person obtaining a copy of  this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions: The above copyright notice and this
permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
// Thank you Tim for your beautiful text editor.  Hope you don't mind me incorporating it into a
//  journaling app with a few modifications -- Andrew
// editor
ZenPen = window.ZenPen || {};
ZenPen.editor = (function() {

	// Editor elements
	var headerField, contentField, cleanSlate, lastType, currentNodeList, savedSelection;

	// Editor Bubble elements
	var textOptions, optionsBox, boldButton, italicButton, quoteButton, urlButton, urlInput;

	var composing;
	var entry_id;

	var networkFailedMessage = `Crap. We weren't able to reach the server... But don't panic!
	We'll save whatever you write and update the server when we get a chance.`

	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};

	function noticeUser(message) {
		var html = `<section class="alert-box -success">
			<p>${message}</p>
			<a class="close" href="#">×</a>
		</section>`
		document.querySelector( '#notice-area' ).innerHTML = html;

	}

	var efficientUpdateBubblePosition = debounce(function() {
		updateBubblePosition();
	}, 100);

	function init() {

		composing = false;
		entry_id = (new URL(document.URL)).pathname.match(/\d+/i)[0];
		localStorage[entry_id] = {}

		bindElements();
		createEventBindings();
		loadStateFromServer();

		// Set cursor position
		var range = document.createRange();
		var selection = window.getSelection();
		range.setStart(headerField, 1);
		selection.removeAllRanges();
		selection.addRange(range);

	}


	function createEventBindings() {
		var smallScreen;
		if (!window.matchMedia("(min-width: 480px)").matches) {
			smallScreen = true;
		} else {
			smallScreen = false;
		}

		// Key up bindings

		if ( ZenPen.util.supportsHtmlStorage() ) {

			document.onkeyup = function( event ) {
				if (!smallScreen) { checkTextHighlighting( event ); }
				saveState();
				//TODO: autosave to server on some counted number of keyups to do soft 'realtime' sync
				// make sure to circuit break on bad network responses.
				//  alternatively wait a minute after the last key and sync that, then don't do it again.
			}

		} else {
			document.onkeyup = checkTextHighlighting;
		}

		if (!smallScreen) {
			// Mouse bindings
			document.onmousedown = checkTextHighlighting;
			document.onmouseup = function( event ) {

				setTimeout( function() {
					checkTextHighlighting( event );
				}, 1);
			};

			// Window bindings
			window.addEventListener('resize', efficientUpdateBubblePosition);
			window.addEventListener('scroll', efficientUpdateBubblePosition);

			// Composition bindings. We need them to distinguish
			// IME composition from text selection
		}
		document.addEventListener( 'compositionstart', onCompositionStart );
		document.addEventListener( 'compositionend', onCompositionEnd );
	}

	function bindElements() {

		headerField = document.querySelector( '.header' );
		contentField = document.querySelector( '.content' );
		textOptions = document.querySelector( '.text-options' );

		optionsBox = textOptions.querySelector( '.options' );

		boldButton = textOptions.querySelector( '.bold' );
		boldButton.onclick = onBoldClick;

		italicButton = textOptions.querySelector( '.italic' );
		italicButton.onclick = onItalicClick;

		quoteButton = textOptions.querySelector( '.quote' );
		quoteButton.onclick = onQuoteClick;

		urlButton = textOptions.querySelector( '.url' );
		urlButton.onmousedown = onUrlClick;

		urlInput = textOptions.querySelector( '.url-input' );
		urlInput.onblur = onUrlInputBlur;
		urlInput.onkeydown = onUrlInputKeyDown;
	}

	function checkTextHighlighting( event ) {

		var selection = window.getSelection();


		if ( (event.target.className === "url-input" ||
		    event.target.classList.contains( "url" ) ||
		    event.target.parentNode.classList.contains( "ui-inputs" ) ) ) {

			currentNodeList = findNodes( selection.focusNode );
			updateBubbleStates();
			return;
		}

		// Check selections exist
		if ( selection.isCollapsed === true && lastType === false ) {
			onSelectorBlur();
		}

		// Text is selected
		if ( selection.isCollapsed === false && composing === false ) {

			currentNodeList = findNodes( selection.focusNode );

			// Find if highlighting is in the editable area
			if ( hasNode( currentNodeList, "ARTICLE") ) {
				updateBubbleStates();
				updateBubblePosition();

				// Show the ui bubble
				textOptions.className = "text-options active";
			}
		}

		lastType = selection.isCollapsed;
	}

	function updateBubblePosition() {
		// check for mobile width.
		if (window.screen.width > 480) {
			var selection = window.getSelection();
			var range = selection.getRangeAt(0);
			var boundary = range.getBoundingClientRect();

			textOptions.style.top = boundary.top - 5 + window.pageYOffset + "px";
			textOptions.style.left = (boundary.left + boundary.right)/2 + "px";
		} else {
			textOptions.style.left      = "inherit"
			textOptions.style.top       = "40px"
			textOptions.style.right     = "60px"
			textOptions.style.position  = "fixed"
			textOptions.style.opacity   = 1
		}

	}

	function updateBubbleStates() {

		// It would be possible to use classList here, but I feel that the
		// browser support isn't quite there, and this functionality doesn't
		// warrent a shim.

		if ( hasNode( currentNodeList, 'B') ) {
			boldButton.className = "bold active"
		} else {
			boldButton.className = "bold"
		}

		if ( hasNode( currentNodeList, 'I') ) {
			italicButton.className = "italic active"
		} else {
			italicButton.className = "italic"
		}

		if ( hasNode( currentNodeList, 'BLOCKQUOTE') ) {
			quoteButton.className = "quote active"
		} else {
			quoteButton.className = "quote"
		}

		if ( hasNode( currentNodeList, 'A') ) {
			urlButton.className = "url useicons active"
		} else {
			urlButton.className = "url useicons"
		}
	}

	function onSelectorBlur() {
		if (window.screen.width > 480) {
			textOptions.className = "text-options fade";
			setTimeout( function() {

				if (textOptions.className == "text-options fade") {

					textOptions.className = "text-options";
					textOptions.style.top = '-999px';
					textOptions.style.left = '-999px';
				}
			}, 260 )
		}
	}

	function findNodes( element ) {

		var nodeNames = {};

		// Internal node?
		var selection = window.getSelection();

		while ( element.parentNode ) {

			nodeNames[element.nodeName] = true;
			element = element.parentNode;

			if ( element.nodeName === 'A' ) {
				nodeNames.url = element.href;
			}
		}

		return nodeNames;
	}

	function hasNode( nodeList, name ) {
		return !!nodeList[ name ];
	}

	function loadStateFromServer() {
		console.log("Getting data from server...")

		var request = new XMLHttpRequest();
		request.open('GET', `${window.location.origin}/entries/${entry_id}.json`, true);

		request.onload = function() {
		  if (request.status >= 200 && request.status < 400) {
				var data = JSON.parse(request.responseText);

				localStorage[entry_id + 'server_header'] = data['title'];
				localStorage[entry_id + 'server_content'] = data['body'];
				console.log(`Updated: ${data['updated_at']}`)
				console.log(`Parsed: ${Date.parse(data['updated_at'])}`)

				localStorage[entry_id + 'server_updated_at'] = Date.parse(data['updated_at']);
				loadState();
		  } else {
		    noticeUser(networkFailedMessage);
		  }
		};

		request.onerror = function() {
		  noticeUser(networkFailedMessage);
		};
		request.send();
	}

	function saveAndRedirect() {
		saveToServer(function(data) {
			noticeUser("Journal Entry saved sucessfully!")
			Turbolinks.visit(document.URL.replace(/\/edit.*/,''));
		})
	}

	function saveToServer(success_callback) {
		console.log("PUTting data to the server...")

		var AUTH_TOKEN = document.querySelectorAll('meta[name=csrf-token]')[0].getAttribute('content');
		var entry_json = {
			entry: {
				title: headerField.innerHTML,
				body: contentField.innerHTML,
				journal_id: document.querySelectorAll('select[name=Journal]')[0].value
			}
		}

		var request = new XMLHttpRequest();
		request.open('PUT', `${window.location.origin}/entries/${entry_id}`, true);
		request.setRequestHeader("X-CSRF-Token", AUTH_TOKEN);
		request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

		request.onload = function() {
		  if (request.status >= 200 && request.status < 400) {
				success_callback()
		  } else {
		    console.log("Out of sync.");
		  }
		};
		request.onerror = function() {
		  console.log("Out of sync.");
		};
		request.send(JSON.stringify(entry_json));

	}

	function saveState( event ) {
		console.log("Saving local...")
		localStorage[entry_id + 'local_header' ] = headerField.innerHTML;
		localStorage[entry_id + 'local_content' ] = contentField.innerHTML;

		// Get epoch time with offset
		localStorage[entry_id + 'local_updated_at' ] = Math.floor((new Date()).getTime() / 1000);
	}

	function loadState() {
		console.log(`Entry ID: ${entry_id}`)
		console.log("Loading State into DOM!")
		console.log("Comparing server_updated_at with local_updated_at ... ... ")
		console.log(`server_updated_at: ${localStorage[entry_id + 'server_updated_at']}`)
		console.log(`local_updated_at: ${localStorage[entry_id + 'local_updated_at']}`)

	  var storage;
	  if ((localStorage[entry_id + 'server_updated_at'] === undefined) ||
	    (localStorage[entry_id + 'server_updated_at'] <= localStorage[entry_id + 'local_updated_at'])) {
			console.log("We've determined that local is fresher")
			noticeUser("Looks like you were working on this earlier... Save it to the server when you're ready.")
	    storage = 'local_';
	  } else {
			console.log("We've determined that the SERVER is fresher")
	    storage = 'server_';
	  }

		console.log(`So our storage is: ${storage}`)

    if (localStorage[entry_id + storage + 'header']) {
      headerField.innerHTML = localStorage[entry_id + storage + 'header'];
    } else {
			console.log("Loading default header")
      headerField.innerHTML = defaultTitle; // in default.js
    }

    if (localStorage[entry_id + storage + 'content'] !== 'null' &&
				localStorage[entry_id + storage + 'content'] !== '' &&
				localStorage[entry_id + storage + 'content']) {
      contentField.innerHTML = localStorage[entry_id + storage + 'content'];
    } else {
			console.log("Loading default content")
      loadDefaultContent()
    }
	}

	function loadDefault() {
		headerField.innerHTML = defaultTitle; // in default.js
		loadDefaultContent();
	}

	function loadDefaultContent() {
		contentField.innerHTML = defaultContent; // in default.js
	}

	function onBoldClick() {
		document.execCommand( 'bold', false );
	}

	function onItalicClick() {
		document.execCommand( 'italic', false );
	}

	function onQuoteClick() {

		var nodeNames = findNodes( window.getSelection().focusNode );

		if ( hasNode( nodeNames, 'BLOCKQUOTE' ) ) {
			document.execCommand( 'formatBlock', false, 'p' );
			document.execCommand( 'outdent' );
		} else {
			document.execCommand( 'formatBlock', false, 'blockquote' );
		}
	}

	function onUrlClick() {

		if ( optionsBox.className == 'options' ) {

			optionsBox.className = 'options url-mode';

			// Set timeout here to debounce the focus action
			setTimeout( function() {

				var nodeNames = findNodes( window.getSelection().focusNode );

				if ( hasNode( nodeNames , "A" ) ) {
					urlInput.value = nodeNames.url;
				} else {
					// Symbolize text turning into a link, which is temporary, and will never be seen.
					document.execCommand( 'createLink', false, '/' );
				}

				// Since typing in the input box kills the highlighted text we need
				// to save this selection, to add the url link if it is provided.
				lastSelection = window.getSelection().getRangeAt(0);
				lastType = false;

				urlInput.focus();

			}, 100);

		} else {

			optionsBox.className = 'options';
		}
	}

	function onUrlInputKeyDown( event ) {

		if ( event.keyCode === 13 ) {
			event.preventDefault();
			applyURL( urlInput.value );
			urlInput.blur();
		}
	}

	function onUrlInputBlur( event ) {

		optionsBox.className = 'options';
		applyURL( urlInput.value );
		urlInput.value = '';

		currentNodeList = findNodes( window.getSelection().focusNode );
		updateBubbleStates();
	}

	function applyURL( url ) {

		rehighlightLastSelection();

		// Unlink any current links
		document.execCommand( 'unlink', false );

		if (url !== "") {

			// Insert HTTP if it doesn't exist.
			if ( !url.match("^(http|https)://") ) {

				url = "http://" + url;
			}

			document.execCommand( 'createLink', false, url );
		}
	}

	function rehighlightLastSelection() {

		window.getSelection().addRange( lastSelection );
	}

	function onCompositionStart ( event ) {
		composing = true;
	}

	function onCompositionEnd (event) {
		composing = false;
	}

	return {
		init: init,
		saveState: saveState,
		saveAndRedirect: saveAndRedirect
	}

})();
