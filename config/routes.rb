Rails.application.routes.draw do
  resources :comments
  get 'welcome/index'

  devise_for :authors

  resources :authors, only: [:new, :create, :index, :show] do
    resources :journals, only: [:index, :new]
  end

  resources :journals do
    resources :entries, only: [:index, :new]
  end

  resources :entries, only: [:show, :update, :destroy, :edit, :new, :create] do
    resources :comments, only: [:new, :create, :index]
  end

  resources :comments, only: [:show, :update, :destroy, :edit]

  root 'welcome#index'

  get 'mydata', to: 'authors#my_data', format: :json

  devise_scope :author do
    get 'login', to: 'devise/sessions#new', as: :login
    get 'signup', to: 'authors/registrations#new', as: :register
    get 'authors/edit', to: 'authors/registrations#edit', as: :edit
    put 'authors', to: 'authors/registrations#edit', as: :update
  end
end
