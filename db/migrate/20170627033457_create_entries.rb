class CreateEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :entries do |t|

      t.text :title, null: false, default: ''
      t.text :body, null: false, default: ''
      t.integer :visibility, null: false, default: 1

      t.belongs_to :journal

      t.timestamps
    end

    add_foreign_key :entries, :journals
  end
end
