class CreateJournals < ActiveRecord::Migration[5.1]
  def change
    create_table :journals do |t|
      t.string  :title
      t.belongs_to :author
      t.integer :visibility, null: false, default: 1
      t.boolean :locked, null: false, default: false

      t.timestamp :active_at

      t.timestamps
    end

    add_foreign_key :journals, :authors
    add_index :journals, [:author_id, :locked], where: "locked = true", unique: true
  end
end
