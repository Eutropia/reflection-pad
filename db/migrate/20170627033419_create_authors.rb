class CreateAuthors < ActiveRecord::Migration[5.1]
  def change
    create_table :authors do |t|
      t.string :name, null: false, default: "Anonymous"
      t.text :bio, null: false, default: "I'm new around here."
      t.timestamp :active_at

      t.timestamps
    end
  end
end
