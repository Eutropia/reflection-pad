class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.belongs_to :author
      t.belongs_to :entry
      t.integer :visibility, null: false, default: 1
      
      t.string :body, null: false, limit: 160

      t.timestamps
    end
  end
end
